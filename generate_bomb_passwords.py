from itertools import permutations
import itertools

first  = ['Public speaking is very easy.']
second = ['1 2 6 24 120 720']

third  = ['0 q 777',
         '1 b 214',
         '2 b 755',
         '3 k 251',
         '4 o 160']

fourth = ['9']

fifth  = ['opekma',
         'opukma',
         'opekmq',
         'opukmq']

six_pos = permutations([0, 1, 2, 3, 4, 5, 6], 6)
sixth_m = list(six_pos)

sixth = []
for pswd in sixth_m:
    l = ''.join([str(el) for el in pswd])
    sixth.append(l)

a = [first, second, third, fourth, fifth, sixth]
res = list(itertools.product(*a))

with open('tor_passwords', 'w') as out:
    for pswd in res:
        line = [''.join(i.split()) for i in pswd]
        print(''.join(line), file=out)