from itertools import permutations
import pexpect
from threading import Thread, Lock

firts  = 'Public speaking is very easy.'
second = '1 2 6 24 120 720'
third  = '0 q 777'
fourth = '9'
fifth  = 'opukmq'

my_file = open("phase_6_passwords", "w")
mutex = Lock()

six_pos = permutations([0, 1, 2, 3, 4, 5, 6], 6)
all = list(six_pos)
lens = len(all)

THREAD_NUM = 8

start = 0
end = lens // THREAD_NUM

def processData(start, end):
    for password_seq in list(all[start : end]):
        analyzer = pexpect.spawnu('./bomb')
        analyzer.sendline(firts)
        analyzer.sendline(second)
        analyzer.sendline(third)
        analyzer.sendline(fourth)
        analyzer.sendline(fifth)

        seq = ' '.join(str(i) for i in password_seq)
        analyzer.sendline(seq)

        analyzer.wait()
        if analyzer.exitstatus == 0:
            with mutex:
                print(seq, file=my_file)
        analyzer.terminate()

for i in range(THREAD_NUM):
    if i != (THREAD_NUM - 1):
       Thread(target = processData, args = (start, end)).start()
    else:
        Thread(target = processData, args = (start, lens)).start()
    start = end
    end += lens // THREAD_NUM