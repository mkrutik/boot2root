from itertools import permutations
import hashlib

uknown_comb = 'erIhtpa'
ending = 'wnage'
gen = permutations(uknown_comb, len(uknown_comb))
gen_l = list(gen)

with open('fun_passwords', 'w') as out_psswd:
    with open('fun_comb', 'w') as out_comb:
        for el in gen_l:
            comb = ''.join(el) + ending
            print(comb, file=out_comb)
            hash = hashlib.sha256(comb.encode()).hexdigest()
            print(hash, file=out_psswd)